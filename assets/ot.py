import numpy as np
from scipy.special import logsumexp
import itertools
import warnings
import matplotlib
import matplotlib.cm as cm
import scipy.optimize

# A little warning regarding matrix rank is a nuissance here
warnings.filterwarnings("ignore")




#1
def getRowSumMatrix(M,N):
    result=np.zeros((M,M*N),dtype=np.double)
    for i in range(M):
        result[i,i*N:(i+1)*N]=1.
    return result
def getColSumMatrix(M,N):
    result=np.zeros((N,M*N),dtype=np.double)
    for i in range(N):
        result[i,i::N]=1.
    return result
def getConstraintMatrix(M,N):
    return np.vstack((getRowSumMatrix(M,N),getColSumMatrix(M,N)))

def solveOT(X,r,Y,s):
    M = len(X)
    N = len(Y)
    c = np.zeros((M,N))
    for i in range(M):
        for j in range(N):
            c[i,j] = np.linalg.norm(X[i]-Y[j])**2
    A=getConstraintMatrix(M,N)
    masses=np.concatenate((r,s))
    
    result=scipy.optimize.linprog(c.ravel(),
                                  A_ub=None, b_ub=None,
                                  A_eq=A, b_eq=masses,
                                  bounds=[(0,None) for i in range(M*N)],
                                  method='revised simplex',
                                  callback=None,
                                  options=None,
                                  x0=None)
    gammaNew=result["x"].reshape((M,N))
    cost = 0
    for i in range(M):
        for j in range(N):
            cost += c[i,j]*gammaNew[i,j]
    
    return (cost,gammaNew)

#3
def getCouplingLines(gamma,X,Y,thresh=1E-15,lwscale=5.,**kwargs):
    """Extract set of lines between point cloudsX and Y according to transport plan gamma,which is given as dense numpy array.Entries below thresh are treated as zero, linewidth is given by lwscale*mass/max(mass) for each entry.kwargs are forwarded to matplotlib.collections.LineCollection."""
    dim=X.shape[1]
    linkRow,linkCol=np.where(gamma>=thresh)
    linkArray=np.zeros((len(linkRow),2,dim),dtype=np.double)
    linkArray[:,0,:]=X[linkRow]
    linkArray[:,1,:]=Y[linkCol]
    linkWeights=gamma[linkRow,linkCol]
    linkWeights=linkWeights/np.max(linkWeights)
    lineCollection=matplotlib.collections.LineCollection(linkArray,lw=lwscale*linkWeights,**kwargs)
    return lineCollection

class partial_multi_L1:
    def __init__(self, marginals, edges, costs, sigmas, epsi, lam, lamp=float("inf"), reparametrize=True):
        """
        marginals should be a list containing the marginal distributions (mu) of every vertex as 1d arrays
        edges should be a list containing tuples describing the edges of the graph, e.g. [(0,1), (1,2), (1,3)]; the entries are indices of marginals; edges should point away from the root (which may be arbitrary)
        costs should be a list containing a cost matrix for each edge; the shape of a cost matrix for edge (u,v) should be (len(marinals[u]), len(marginals[v]))
        sigmas should be a list of the same shape as costs containing distributions to use in KL-divergence
        epsi is the strength of entropic regularization
        lam is the cost of unmatched mass
        lamp is the cost of virtual mass
        """
        # number of spaces (= vertices in the graph)
        self.n = len(marginals)
        # list of marginals
        self.marginals = marginals
        if not reparametrize:
            self.logmarginals = [np.log(marginals[i]) for i in range(self.n)]
        # dimensions of marginals
        self.dims = [len(m) for m in marginals]
        # list of edges
        self.edges = edges
        # dictionary of costs (given edges)
        if reparametrize:
            self.costs = {edges[i] : np.exp(-costs[i] / epsi) * sigmas[i] for i in range(len(edges))}
        else:
            self.costs = {edges[i] : costs[i] for i in range(len(edges))}
        # dictionary of sigmas (given edges)
        self.sigmas = {edges[i] : sigmas[i] for i in range(len(edges))}
        # list of outgoing edges given a vertex
        self.out = [[e for e in edges if e[0] == u] for u in range(self.n)]
        # list of incoming edges given a vertex
        self.inc = [[e for e in edges if e[1] == u] for u in range(self.n)]
        # list of vertex degrees
        self.degs = [len(self.inc[u]) + len(self.out[u]) for u in range(self.n)]
        
        self.epsi = epsi
        self.lam = lam
        self.lamp = lamp
        
        # dual variables
        if reparametrize:
            self.y = {e : [np.ones(self.dims[e[0]]), np.ones(self.dims[e[1]])] for e in edges}
        else:
            self.y = {e : [np.zeros(self.dims[e[0]]), np.zeros(self.dims[e[1]])] for e in edges}

        self.reparametrize = reparametrize
    
    def set_epsi(self, new_epsi):
        if self.reparametrize:
            for e in self.edges:
                for i in [0,1]:
                    self.y[e][i] = self.y[e][i]**(self.epsi / new_epsi)
                self.costs[e] = self.costs[e]**(self.epsi / new_epsi)
        self.epsi = new_epsi
        
    def partial_step(self, u):
        """
        do one partial sinkhorn step for vertex u
        """

        if self.reparametrize:
            t = []
            sumlogt = np.zeros(self.dims[u])
        
            for e in self.out[u]:
                t += [self.marginals[u] / np.sum(self.y[e][1].reshape((1,-1)) * self.costs[e], axis=1)]
                sumlogt += np.log(t[-1])
            
            for e in self.inc[u]:
                t += [self.marginals[u] / np.sum(self.y[e][0].reshape((-1,1)) * self.costs[e], axis=0)]
                sumlogt += np.log(t[-1])

            scale = np.minimum(0, self.lam / self.epsi - sumlogt)
            if self.lamp > 0 and self.lamp < float("inf"):
                scale = np.maximum(scale, -self.lamp / self.epsi - sumlogt)
                
            scale = np.exp(scale / self.degs[u])
        
            step_size = 0
            i = 0
            for e in self.out[u]:
                step_size += np.sum((self.y[e][0] - t[i] * scale)**2)
                self.y[e][0] = t[i] * scale
                i += 1
                
            for e in self.inc[u]:
                step_size += np.sum((self.y[e][1] - t[i] * scale)**2)
                self.y[e][1] = t[i] * scale
                i += 1
        else:
            t = []
            sumt = np.zeros(self.dims[u])

            for e in self.out[u]:
                t += [self.epsi * (self.logmarginals[u] - logsumexp((self.y[e][1].reshape((1,-1)) - self.costs[e]) / self.epsi, axis=1, b = self.sigmas[e]))]
                sumt += t[-1]

            for e in self.inc[u]:
                t += [self.epsi * (self.logmarginals[u] - logsumexp((self.y[e][0].reshape((-1,1)) - self.costs[e]) / self.epsi, axis=0, b = self.sigmas[e]))]
                sumt += t[-1]

            offset = np.minimum(0, self.lam - sumt)
            if self.lamp > 0 and self.lamp < float("inf"):
                offset = np.maximum(offset, -self.lamp - sumt)
            offset = offset / self.degs[u]

            step_size = 0
            i = 0
            for e in self.out[u]:
                step_size += np.sum((self.y[e][0] - (t[i] + offset))**2)
                self.y[e][0] = t[i] + offset
                i += 1

            for e in self.inc[u]:
                step_size += np.sum((self.y[e][1] - (t[i] + offset))**2)
                self.y[e][1] = t[i] + offset
                i += 1
            
        return step_size
        
    def step(self):
        """
        do one complete sinkhorn step
        """
        
        step_size = 0
        for u in range(self.n):
            step_size += self.partial_step(u)
        return step_size
    
    def run(self, max_iterations=1000, min_step_size=1e-3, check_interval=1, verbose_output=False):
        it = 0
        crit = ""
        step_sizes = []
        primal_step_sizes = []
        coupling_ranges = []
        coupling_strengths = []
        old_primal = self.get_primal()
        primals = []
        while True:
            it += 1
            step_sizes += [self.step()**.5]

            if it % check_interval == 0 or verbose_output:
                new_primal = self.get_primal()
                primal_step_sizes += [self.primal_dist(old_primal, new_primal)]
            
            if (it + 1) % check_interval == 0:
                old_primal = self.get_primal() if check_interval > 1 else new_primal
        
            if verbose_output:
                primals += [new_primal]
                couplings = [np.sum(pi) for pi in new_primal]
                coupling_ranges += [max(couplings) - min(couplings)]
                coupling_strengths += [np.mean(couplings)]

            if it % check_interval == 0 and primal_step_sizes[-1] < min_step_size:
                crit = "minimal step size reached"
                break
            if it >= max_iterations:
                crit = "maximal number of iterations reached"
                break

        if not verbose_output:
            return {"end_criterion" : crit, 
                    "iterations" : it,
                    "step_sizes" : step_sizes,
                    "primal_step_sizes" : primal_step_sizes}
        else:
            return {"end_criterion" : crit, 
                    "iterations" : it,
                    "step_sizes" : step_sizes,
                    "primal_step_sizes" : primal_step_sizes,
                    "coupling_ranges" : coupling_ranges,
                    "coupling_strengths" : coupling_strengths,
                    "primals" : primals}
    
    def run_epsi_scale(self, epsi_start=1, stages=10, max_iterations=1000, check_interval=1, min_step_size=1e-4, verbose_output=False):
        epsis = np.logspace(np.log(epsi_start), np.log(self.epsi), stages, base=np.e)
            
        crits = []
        its = []
        step_sizes = []
        primal_step_sizes = []
        coupling_ranges = []
        coupling_strengths = []
        primals = []
        
        for epsi in epsis:
            self.set_epsi(epsi)
            r = self.run(max_iterations, min_step_size, verbose_output=verbose_output, check_interval=check_interval)
            crits += [r["end_criterion"]]
            its += [r["iterations"]]
            step_sizes += [r["step_sizes"]]
            primal_step_sizes += [r["primal_step_sizes"]]
            if verbose_output:
                coupling_ranges += [r["coupling_ranges"]]
                coupling_strengths += [r["coupling_strengths"]]
                primals += [r["primals"]]

        if not verbose_output:
            return {"end_criterions" : crits, 
                    "iterations" : its,
                    "step_sizes" : step_sizes,
                    "primal_step_sizes" : primal_step_sizes,
                    "epsis" : epsis}
        else:
            return {"end_criterions" : crits, 
                    "iterations" : its,
                    "step_sizes" : step_sizes,
                    "primal_step_sizes" : primal_step_sizes,
                    "coupling_ranges" : coupling_ranges,
                    "coupling_strengths" : coupling_strengths,
                    "primals" : primals,
                    "epsis" : epsis}
         
    def get_primal(self):
        """
        return current primal
        """
        if self.reparametrize:
            return [self.y[e][0].reshape((-1,1)) * self.y[e][1].reshape((1,-1)) * self.costs[e] * self.sigmas[e] for e in self.edges]
        else:
            return [np.exp((self.y[e][0].reshape((-1,1)) + self.y[e][1].reshape((1,-1)) - self.costs[e]) / self.epsi) * self.sigmas[e] for e in self.edges]
        
    def primal_dist(self, p1, p2):
        """
        return L2 distance between two primal solutions
        """
        return np.sum([np.sum((p1[i] - p2[i])**2) for i in range(len(p1))])**.5
        
    def coupling_strength(self):
        """
        return the total transported mass / coupling strength for each edge
        """
        return [np.sum(pi) for pi in self.get_primal()]
    
    def get_match_counts(self):
        """
        calculate numbers of all sub-graphs
        return match_cnts, match_str, deltas
        where:
        match_cnts = numbers of sub-graphs
        match_str = description of sub-graphs as strings
        deltas = description of sub-graphs as binary tuples
        """
        
        pi = self.get_primal()
        mu = self.marginals
        pi_m = {e : (np.sum(pi[i], axis=1), np.sum(pi[i], axis=0)) for i, e in enumerate(self.edges)}
        mu_tilde = np.array([np.mean([pi_m[e][i]
                                      for i in [0,1]
                                      for e in self.edges
                                      if e[i] == v], axis=0)
                             for v in range(self.n)])
        real = np.minimum(mu_tilde, np.array(mu))
        virt = np.maximum(0, mu_tilde - np.array(mu))
        
        n = self.n
        adj_list = [[] for _ in range(n)]
        parent = [None for _ in range(n)]
        for i, e in enumerate(self.edges):
            adj_list[e[0]] += [(e[1], i)]
            parent[e[1]] = e[0]
            
        root = parent.index(None)

        def dfs_count_matches(w, delta = [0 for _ in range(n)], pei = None):
            """
            w = current vertex
            delta = real / virt mask per vertex (0 = real, 1 = virt)
            pei = index of parent edge
            """
            if pei is not None:
                v = self.edges[pei][0]
                
                rho_w = pi[pei] / (mu_tilde[v].reshape((-1,1)) * mu_tilde[w].reshape((1,-1)))
                
                if delta[w]:
                    rho_w *= virt[w].reshape((1,-1))
                else:
                    rho_w *= real[w].reshape((1,-1))
                    
                for u, ei in adj_list[w]:
                    rho_u = dfs_count_matches(u, delta, ei)
                    rho_w *= rho_u.reshape((1,-1))
            
                return np.sum(rho_w, axis = 1)
            else:
                if delta[w]:
                    rho_w = virt[w].reshape((1,-1)).copy()
                else:
                    rho_w = real[w].reshape((1,-1)).copy()
                    
                for u, ei in adj_list[w]:
                    rho_w *= dfs_count_matches(u, delta, ei)
                    
                return np.sum(rho_w)

        deltas = list(itertools.product(*[[0,1] for _ in range(n)]))
        match_cnts = np.zeros(len(deltas))
        match_str = ["" for _ in deltas]
        
        for i, delta in enumerate(deltas):
            match_cnts[i] = dfs_count_matches(root, delta)
            match_str[i] = "".join([(chr(ord('A') + j) if delta[j] == 0 else "_") for j in range(len(delta))])

        return match_cnts, match_str, deltas

def get_costs(X, edges):
    res = []
    for i, j in edges:
        res += [np.linalg.norm(X[i].reshape((-1, 1, 2)) - X[j].reshape((1, -1, 2)), axis=2)]
    return res
    
def make_sample2d(seed=None, 
                  edges=[(0,1),(1,2)], 
                  common_n=30, 
                  spread=[0.1, 0.1], 
                  root=0,
                  distribution="uniform",
                  distribution_parameters=None,
                  stray=[0,0,0],
                  stray_distribution="uniform",
                  stray_distribution_parameters=None,
                  dropout=[0,0,0],
                  add_ghost_copies=False,
                  ghost_weight=1e-9):
    """
    seed is used to initialize random number generation
    edges describe the graph, should use vertices 0,1,...,m-1
    common_n is the number of true coupled samples
    spread describes the standard deviation along each edge for true coupled samples
    root is the marginal which is created first
    distribution describes the point distribution for root vertices (possible: "uniform", "normal")
    distribution_parameters parametrize the distribution ((a,b),(c,d)) for uniform on [a,b]x[c,d] or [mean, cov] with mean of shape (2,) and cov of shape (2,2) for normal
    stray is how many independent points are added for each marginal
    stray_distribution describes the point distribution of stray points
    dropout is how many true points are invisible for each marginal (dropout is performed after adding strays)
    add_ghost_copies specifies whether all marginals should have the same point set with mu specifying the actual points
    ghost_weight is the weight assigned to ghost points (since 0 can be problematic)
    """
    np.random.seed(seed)
    m = np.max(edges) + 1
    
    # generate root positions
    if distribution == "uniform":
        if distribution_parameters is None:
            distribution_parameters = ((0,1),(0,1))
        x = np.random.uniform(*distribution_parameters[0], size=(common_n,1))
        y = np.random.uniform(*distribution_parameters[1], size=(common_n,1))
        pts = np.hstack((x,y))
    elif distribution == "normal":
        if distribution_parameters is None:
            distribution_parameters = ([0,0], [[1,0],[0,1]])
        pts = np.random.multivariate_normal(*distribution_parameters, size=common_n)
    else:
        raise ValueError(distribution + " is not a valid distribution. Use normal or uniform")
    
    # initialize marginals
    X = [None for _ in range(m)]
    X[root] = pts
    
    # traverse graph and generate other marginals
    edg = [[] for _ in range(m)]
    for i, e in enumerate(edges):
        edg[e[0]] += [(e[1], spread[i])]
        edg[e[1]] += [(e[0], spread[i])]
    
    q = [root]
    while len(q) > 0:
        v = q[0]
        q = q[1:]
        
        for w, s in edg[v]:
            if X[w] is None:
                q += [w]
                X[w] = X[v] + np.random.normal(0, s, size=(common_n,2))
    
    # generate and add stray points
    for i in range(m):
        if stray_distribution == "uniform":
            if stray_distribution_parameters is None:
                stray_distribution_parameters = ((0,1),(0,1))
            x = np.random.uniform(*stray_distribution_parameters[0], size=(stray[i],1))
            y = np.random.uniform(*stray_distribution_parameters[1], size=(stray[i],1))
            pts = np.hstack((x,y))
        elif stray_distribution == "normal":
            if stray_distribution_parameters is None:
                stray_distribution_parameters = ([0,0], [[1,0],[0,1]])
            pts = np.random.multivariate_normal(*stray_distribution_parameters, size=stray[1])
        else:
            raise ValueError(distribution + " is not a valid stray_distribution. Use normal or uniform")
        X[i] = np.vstack((X[i], pts))
    
    # permute samples and perform dropout
    perms = [np.random.permutation(common_n + stray[i]) for i in range(m)]
    iperms = [np.empty(common_n + stray[i], dtype=int) for i in range(m)]
    for i in range(m):
        iperms[i][perms[i]] = np.arange(common_n + stray[i])
    
    truth = [[iperms[i][j] if iperms[i][j] < common_n + stray[i] - dropout[i] else None
              for i in range(m)] 
             for j in range(common_n)]
    
    for i in range(m):
        X[i] = X[i][perms[i]][:common_n + stray[i] - dropout[i]]

    # if requested add ghost copies and calculate mu
    if add_ghost_copies:
        X = [np.concatenate(X) for _ in range(m)]
        k = np.cumsum([0] + [common_n + stray[i] - dropout[i] for i in range(m)])
        mu = [np.full(k[-1], ghost_weight) for _ in range(m)]
        for i in range(m):
            mu[i][k[i]:k[i+1]] = np.ones(common_n + stray[i] - dropout[i])
    else:
        mu = [np.ones(common_n + stray[i] - dropout[i]) for i in range(m)]
    
    # calculate costs and costs
    costs = get_costs(X, edges)
        
    return mu, costs, X, np.array(truth)

def make_sample2d_s(seed=None, 
                    edges=[(0,1),(1,2)], 
                    spread=[0.01, 0.01],
                    species={(0,1,2) : 30, (0,) : 20, (1,) : 20, (2,) : 10},
                    distribution="uniform",
                    distribution_parameters=None,
                    add_ghost_copies=False,
                    ghost_weight=1e-9):
    """
    seed is used to initialize random number generation
    edges describe the graph, should use vertices 0,1,...,m-1
    spread describes the standard deviation along each edge for true coupled samples
    species describes how many of each subtree should be generated
    distribution describes the point distribution for root vertices (possible: "uniform", "normal")
    distribution_parameters parametrize the distribution ((a,b),(c,d)) for uniform on [a,b]x[c,d] or [mean, cov] with mean of shape (2,) and cov of shape (2,2) for normal
    add_ghost_copies specifies whether all marginals should have the same point set with mu specifying the actual points
    ghost_weight is the weight assigned to ghost points (since 0 can be problematic)
    """
    np.random.seed(seed)
    m = np.max(edges) + 1
    
    # generate root positions
    if distribution == "uniform":
        if distribution_parameters is None:
            distribution_parameters = ((0,1),(0,1))
        make_pts = lambda cnt : np.random.uniform(*distribution_parameters[0], size=(cnt,2))
    elif distribution == "normal":
        if distribution_parameters is None:
            distribution_parameters = ([0,0], [[1,0],[0,1]])
        make_pts = lambda cnt : np.random.multivariate_normal(*distribution_parameters, size=cnt)
    else:
        raise ValueError(distribution + " is not a valid distribution. Use normal or uniform")

    pcnt = np.sum(list(species.values()))
    
    # initialize marginals
    X = [None for _ in range(m)]
    root = 0
    X[root] = make_pts(pcnt)

    # collection of segments for plotting (includes deleted points)
    edg_lines = []
    
    # traverse graph and generate other marginals
    edg = [[] for _ in range(m)]
    for i, e in enumerate(edges):
        edg[e[0]] += [(e[1], spread[i])]
        edg[e[1]] += [(e[0], spread[i])]

    q = [root]
    while len(q) > 0:
        v = q[0]
        q = q[1:]
        
        for w, s in edg[v]:
            if X[w] is None:
                q += [w]

                X[w] = X[v] + np.random.normal(0, s, size=(pcnt,2))

                for i in range(pcnt):
                    edg_lines += [(X[v][i].copy(), X[w][i].copy())]

    # set unwanted points to NaN
    ccnt = 0
    for s in species:
        scnt = species[s]
        for i in [j for j in range(m) if j not in s]:
            X[i][ccnt:ccnt+scnt] = float("NaN")
        ccnt += scnt
    
    rawX = X.copy()
    
    true_coupling = [np.eye(ccnt) for _ in edges]
    
    for i in range(m):
        for j, e in enumerate(edges):
            if e[0] == i:
                true_coupling[j] = true_coupling[j][~np.isnan(X[i]).any(axis=1),:]
            if e[1] == i:
                true_coupling[j] = true_coupling[j][:,~np.isnan(X[i]).any(axis=1)]
    
    # remove NaN points
    X = [X[i][~np.isnan(X[i]).any(axis=1)] for i in range(m)]
    
    # if requested add ghost copies and calculate mu
    if add_ghost_copies:
        k = np.cumsum([0] + [len(X[i]) for i in range(m)])
        mu = [np.full(k[-1], ghost_weight) for _ in range(m)]
        for i in range(m):
            mu[i][k[i]:k[i+1]] = 1
        X = [np.concatenate(X) for _ in range(m)]
        
        p_true_coupling = true_coupling
        true_coupling = [np.zeros((k[-1],k[-1])) for _ in edges]
        for j, e in enumerate(edges):
            true_coupling[j][k[e[0]]:k[e[0]+1], k[e[1]]:k[e[1]+1]] = p_true_coupling[j]
    else:
        mu = [np.ones(len(X[i])) for i in range(m)]
    
    # calculate costs and costs
    costs = get_costs(X, edges)
    
    # calculate true coupling
    truth = [np.zeros_like(c) for c in costs]
    for i, e in enumerate(edges):
        truth[i]
    
    return mu, costs, X, true_coupling, edg_lines

