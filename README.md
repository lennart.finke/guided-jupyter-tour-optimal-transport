# Guided Jupyter-Tour: Optimal Transport
CRC1456 Binder: [![Binder](https://mybinder.org/badge_logo.svg)](http://c111-004.cloud.gwdg.de:30901/v2/gwdg/lennart.finke%2Fguided-jupyter-tour-optimal-transport/HEAD?urlpath=voila)

Mybinder.org: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flennart.finke%2Fguided-jupyter-tour-optimal-transport/HEAD?urlpath=voila)

In this collection of 4 Jupyter notebooks, the reader is gently and informally introduced to selected topics of Optimal Transport and Particle Colocalization. They can be accessed through here:


1. [**Introduction: But what *is* Optimal Transport?** <br> ![](assets/Dirt.png)](Intro.ipynb)
2. [**Pixel-based Particle Colocalization** <br> <img src='assets/Pixels.png' width='431px' height=280px style=''/>](Pointwise_Correlation.ipynb) 
3. [**Connecting the Dots with Optimal Transport - Particle Colocalization** <br> <img src='assets/Particles.png'/>](Colocalization_with_Optimal_Transport.ipynb)
4. [**The Patient Space - Spotting Alzheimer's Disease with optimal Transport**  <br> <img src='assets/Cells.jpg' width='431px' />](Optimal_Transport_and_X-ray_Tomography.ipynb)\[1\]


The work of the University of Göttingen's [Collaborative Research Center 1456](https://www.uni-goettingen.de/en/628179.html) is especially in focus and provides the context from which to understand and motivate the material. Readers are encouraged to play with and modify the code for themselves.

**Audience -** The notebooks were written for intermediate Bachelor's students in mathematics or Computer Science. Especially notebooks 3 and 4 treat recent research and may be interesting for Master's students and above, too. Audiences without mathematical background are welcome to skip more technical explanations and focus on the introductions, interactive widgets and graphs. These are purpusefully made accessible without much in the way of mathematical background knowledge.

**Methods -** The notebooks are written in Python 3 with Jupyter and `ipywidgets` as the interactive elements. They utilize `numpy` and `scipy`. A library for solving optimal transport problems developed by Prof. Bernhard Schmitzer was also used, which is open source and can be found [here](https://github.com/bernhard-schmitzer/UnbalancedLOT).

**Project Duration -** The itself project was put together from May to July 2022. It builds on previous work in the form of Bachelor's theses by Fabian Fieberg and Thilo Stier.

**Authorship -** Programming: Thilo Stier, Fabian Fieberg and Lennart Finke. Text: Lennart Finke. Supervised by Prof. Dr. Cristoph Lehrenfeld & Prof. Dr. Bernhard Schmitzer. Images

**License -** the notebooks themselves are licenced under MIT. For the images' license, see the note below. The library mentioned above is also included here, under `lib/` and `data/`. Licensed under [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause), © 2021 Bernhard Schmitzer.

****

[1] From the study *Three-dimensional virtual histology of the human hippocampus based on phase-contrast computed tomography* published in *Proceedings of the National Academy of Sciences* Vol. 118 under the [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) license, © 2021 the Authors. Special thanks to Prof. Dr. Tim Salditt, by whom permission of use was kindly given.
